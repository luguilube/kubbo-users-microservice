## Kubbo Test App

Kubbo Test App es una app basada en una idea inicial de la empresa Kubbo, específicamente como prueba técnica para postulación a una vacante. Esta app fue diseñada en un principio por un conjunto de 6 microservicios para el backend  (servidor, customers, products, warehouses, orders, cloud gateway) para gestionar específicamente lo referente a cada funcionalidad requerida por la App, pero segmentada en módulos; y para el frontend un proyecto únicamente. Actualmente se sigue actualizando que nuevas funcionalidades y tecnologías para ser mostrada como proyecto de portafolio. 

## Descripción del Proyecto

Toda la descripción detallada del proyecto, se encuentra dentro del repositorio de [kubbo-test-app-frontend](https://bitbucket.org/luguilube/kubbo-test-app-frontend/src/master/) 

## Kubbo Users Microservice

Microservicio que permite todas las operaciones CRUD para la entidad Usuarios, este microservicio está pensado para una posterior implementación de autenticación usando Spring Security y JWT.

## Nota

Se recomienda que este microservicio, sea el cuarto en ser ejecutado ya que depende de la ejecución de los microservicios:

- kubbo-config-server.
- kubbo-eureka-server.