FROM openjdk:11

VOLUME /tmp

ADD ./target/kubbo-users-microservice-0.0.1-SNAPSHOT.jar kubbo-users-microservice.jar

ENTRYPOINT ["java", "-jar", "/kubbo-users-microservice.jar"]