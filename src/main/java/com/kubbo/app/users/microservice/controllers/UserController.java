package com.kubbo.app.users.microservice.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.kubbo.app.users.microservice.models.entities.User;
import com.kubbo.app.users.microservice.models.services.UserService;

@RestController
public class UserController {
	private static final Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService service;
	
	@GetMapping("/list")
	public ResponseEntity<?> list() {
		try {
            return ResponseEntity.ok().body(service.findAll());
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
	
	@GetMapping("/get-by-username/{username}")
	public ResponseEntity<?> getByUsername(@PathVariable String username) {
		try {
            User user = service.findByUsername(username);

            if (user == null) {
                return ResponseEntity.notFound().build();
            }

            return ResponseEntity.ok().body(user);
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
	
	@GetMapping("/get-by-id/{id}")
	public ResponseEntity<?> getById(@PathVariable Long id) {
		try {
            User user = service.findById(id);

            if (user == null) {
                return ResponseEntity.notFound().build();
            }

            return ResponseEntity.ok().body(user);
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
	
	@PostMapping("/store")
	public ResponseEntity<?> store(@Valid @RequestBody User user, BindingResult result) {
		try {
            if (result.hasErrors()) {
                return validation(result);
            }

            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(user));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
	
	@PutMapping("/update/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> update(@Valid @RequestBody User user, @PathVariable Long id, BindingResult result) {
		
		try {
            if (result.hasErrors()) {
                return validation(result);
            }
            
            User UserToUpdate = service.findById(id);
            
            if (UserToUpdate == null) {
            	return ResponseEntity.notFound().build();
            }
            
            UserToUpdate.setName(user.getName());
            UserToUpdate.setSurname(user.getSurname());
            UserToUpdate.setEmail(user.getEmail());
            UserToUpdate.setEnabled(user.getEnabled());
            UserToUpdate.setAttempts(user.getAttempts());
            
            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(UserToUpdate));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
        	return service.deleteById(id) 
            		? ResponseEntity.noContent().build() 
    				: ResponseEntity.notFound().build();
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
	
    private ResponseEntity<?> validation(BindingResult result) {
        Map<String, Object> errors = new HashMap<>();

        result.getFieldErrors().forEach(err -> {
            errors.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
        });

        return ResponseEntity.badRequest().body(errors);
    }
}
