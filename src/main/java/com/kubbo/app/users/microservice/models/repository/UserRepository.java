package com.kubbo.app.users.microservice.models.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.kubbo.app.users.microservice.models.entities.User;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	public List<User> findAll();
	public Optional<User> findById(Long id);
	public Optional<User> findByUsername(String username);
}
