package com.kubbo.app.users.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class KubboUsersMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubboUsersMicroserviceApplication.class, args);
	}

}
