package com.kubbo.app.users.microservice.models.services;

import java.util.List;

import com.kubbo.app.users.microservice.models.entities.User;

public interface UserService {
	public List<User> findAll();
	public User findById(Long id);
	public User findByUsername(String username);
	public User save(User user);
	public boolean deleteById(Long id);
}
