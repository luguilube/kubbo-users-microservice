INSERT INTO kubbo_test_app.users (name, surname, username, password, dni, phone, email, birthdate, address, created_at, enabled, deleted, attempts) VALUES ('Rosa', 'Angela', 'rosa', '$2y$12$9kXHrDUUhWU3Yx4I5KNjk.LdzpVzSUkrzbfBpJxc/4oqrN9E6uuea', 'Y1785AC', '678954123', 'rosangela.ing10@gmail.com', '1992-06-10', 'Lord Cochrane 302', NOW(), 1, 0, 0);
INSERT INTO kubbo_test_app.users (name, surname, username, password, dni, phone, email, birthdate, address, created_at, enabled, deleted, attempts) VALUES ('Administrador', 'Del Sistema', 'admin', '$2y$12$9kXHrDUUhWU3Yx4I5KNjk.LdzpVzSUkrzbfBpJxc/4oqrN9E6uuea', 'Y123789753A', '195756456', 'elluisluna0@gmail.com', '1990-10-10', 'Alguna direccion', NOW(), 1, 0, 0);

INSERT INTO kubbo_test_app.roles (name, description, created_at) VALUES ('ROLE_USER', 'Usuario general del sistema', NOW());
INSERT INTO kubbo_test_app.roles (name, description, created_at) VALUES ('ROLE_ADMIN', 'Adminsitrador general del sistema', NOW());

INSERT INTO kubbo_test_app.roles_users (user_id, role_id) VALUES (1,1);
INSERT INTO kubbo_test_app.roles_users (user_id, role_id) VALUES (2,1);
INSERT INTO kubbo_test_app.roles_users (user_id, role_id) VALUES (2,2);